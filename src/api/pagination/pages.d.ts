import Page from "./page";

export default interface Pages {
  count: number;
  perPage: number;

  iterator: IterableIterator<Page>;
}
