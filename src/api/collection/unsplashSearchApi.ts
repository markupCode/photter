import Unsplash, { toJson } from "unsplash-js";
import AdapterFactory from "../../adapters/adapterFactory";
import Collection from "../../models/collection";
import PaginationOptions from "../pagination/paginationOptions";
import UnsplashPages from "../pagination/unsplashPages";
import SearchApi from "./searchApi";

export default class UnsplashSearchApi implements SearchApi {
  private client: Unsplash;
  private adapters: AdapterFactory;
  private options: PaginationOptions;

  constructor(
    client: Unsplash,
    adapters: AdapterFactory,
    options: PaginationOptions
  ) {
    this.client = client;
    this.adapters = adapters;
    this.options = options;
  }

  public async byId(id: number): Promise<Collection> {
    const json = await toJson(await this.client.collections.getCollection(id));

    return this.adapters.getCollectionAdapter(json);
  }

  public async byName(name: string, count: number = 10): Promise<Collection[]> {
    const search = this.client.search.collections;
    const collections = [];

    const availableCount = await this.getAvailableItems(name);
    const receivedCount = Math.min(count, availableCount);

    const pages = new UnsplashPages(receivedCount, this.options);

    for (const page of pages.iterator) {
      const json = await toJson(await search(name, page.number, pages.perPage));
      collections.push(...json.results);
    }

    return this.mapJsonToCollections(collections.slice(0, receivedCount));
  }

  private async getAvailableItems(query: string): Promise<number> {
    const json = await toJson(
      await this.client.search.collections(query, 1, 0)
    );
    return parseInt(json.total, 10);
  }

  private mapJsonToCollections(json: any): Collection[] {
    const collections = new Array<Collection>();
    json.forEach(item => {
      collections.push(this.adapters.getCollectionAdapter(item));
    });

    return collections;
  }
}
