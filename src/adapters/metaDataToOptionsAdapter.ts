import MetaData from "../models/metaData";
import Options from "../models/options";

export default class MetaDataToOptionsAdapter extends Options {
  get width(): number {
    return this.source.size.width;
  }
  set width(value) {
    this.source.size.width = value;
  }

  get height(): number {
    return this.source.size.height;
  }
  set height(value) {
    this.source.size.height = value;
  }

  get orientation(): string {
    return this.source.size.ratio.type.toString();
  }

  get query(): string {
    return this.source.tags.combine();
  }

  protected source: MetaData;

  constructor(metaData: MetaData) {
    super();
    this.source = metaData;
  }
}
