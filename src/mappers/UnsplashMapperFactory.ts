import Collection from "../models/collection";
import Photo from "../models/photo";
import PhotoLinks from "../models/photoLinks";
import User from "../models/user";
import MapperFactory from "./mapperFactory";

export default class UnsplashMapperFactory implements MapperFactory {
  /**
   * represent mapping between`User` core class
   * and `Unsplash.User` from **Unsplash-JS**
   */
  public mapUserToJson(user: User): any {
    return {
      bio: user.bio,
      id: user.id,
      location: user.location,
      name: user.name,
      portfolio_url: user.portfolioUrl,
      username: user.username
    };
  }

  /**
   * represent mapping between`PhotoLinks` core class
   * and `Unsplash.Photo.Links` from **Unsplash-JS**
   */
  public mapPhotoLinksToJson(links: PhotoLinks): any {
    return {
      download: links.download,
      download_location: links.downloadLocation,
      html: links.html,
      self: links.self
    };
  }

  /**
   * represent mapping between`Photo` core class
   * and `Unsplash.Photo` from **Unsplash-JS**
   */
  public mapPhotoToJson(photo: Photo): any {
    return {
      color: photo.user,
      description: photo.description,
      height: photo.height,
      id: photo.id,
      links: this.mapPhotoLinksToJson(photo.links),
      urls: photo.urls,
      user: this.mapUserToJson(photo.user),
      width: photo.width
    };
  }

  /**
   * represent mapping between`Collection` core class
   * and `Unsplash.Collection` from **Unsplash-JS**
   */
  public mapCollectionToJson(collection: Collection): any {
    return {
      description: collection.description,
      id: collection.id,
      links: collection.links,
      shareKey: collection.shareKey,
      title: collection.title,
      totalPhotos: collection.totalPhotos,
      user: this.mapUserToJson(collection.user)
    };
  }
}
