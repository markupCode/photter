import PhotoLinks from "../models/photoLinks";

/**
 * represent mapping between`Unsplash.Photo.Links`
 * from **Unsplash-JS** and core `PhotoLinks` class
 */
export default class JsonToPhotoLinksAdapter implements PhotoLinks {
  get self(): string {
    return this.source.self;
  }
  set self(value) {
    this.source.self = value;
  }

  get html(): string {
    return this.source.html;
  }
  set html(value) {
    this.source.html = value;
  }

  get download(): string {
    return this.source.download;
  }
  set download(value) {
    this.source.download = value;
  }

  get downloadLocation(): string {
    return this.source.download_location;
  }
  set downloadLocation(value) {
    this.source.download_location = value;
  }

  protected source: any;

  constructor(source: any) {
    this.source = source;
  }
}
