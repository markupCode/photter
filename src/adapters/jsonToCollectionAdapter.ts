import Collection from "../models/collection";
import CollectionLinks from "../models/collectionLinks";
import User from "../models/user";

/**
 * represent mapping between`Unsplash.Collection`
 * from **Unsplash-JS** and core `Collection` class
 */
export default class JsonToCollectionAdapter implements Collection {
  get id(): number {
    return this.source.id;
  }
  set id(value) {
    this.source.id = value;
  }

  get title(): string {
    return this.source.title;
  }
  set title(value) {
    this.source.title = value;
  }

  get description(): string {
    return this.source.description;
  }
  set description(value) {
    this.source.description = value;
  }

  get totalPhotos(): number {
    return this.source.total_photos;
  }
  set totalPhotos(value) {
    this.source.totalP_photos = value;
  }

  get shareKey(): string {
    return this.source.share_key;
  }
  set shareKey(value) {
    this.source.share_key = value;
  }

  get user(): User {
    return this._user;
  }
  set user(value) {
    this._user = value;
  }

  get links(): CollectionLinks {
    return this.source._links;
  }
  set links(value) {
    this.source._links = value;
  }

  protected source: any;

  private _user: User;
  private _links: CollectionLinks;

  constructor(source: any, user: User) {
    this.source = source;
    this.setInnerAdapters(user);
  }

  protected setInnerAdapters(user: User) {
    this._user = user;
  }
}
