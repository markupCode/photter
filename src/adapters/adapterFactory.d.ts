import User from "../models/user";
import PhotoLinks from "../models/photoLinks";
import CollectionLinks from "../models/collectionLinks";
import Urls from "../models/urls";
import Collection from "../models/collection";
import Photo from "../models/photo";

export default interface AdapterFactory {
  getPhotoAdapter(source: any): Photo;
  getUserAdapter(source: any): User;
  getPhotoLinksAdapter(source: any): PhotoLinks;
  getCollectionAdapter(source: any): Collection;
}
