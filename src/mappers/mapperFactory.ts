import Collection from "../models/collection";
import Photo from "../models/photo";
import PhotoLinks from "../models/photoLinks";
import User from "../models/user";

export default interface MapperFactory {
  mapPhotoToJson(photo: Photo): any;
  mapCollectionToJson(collection: Collection): any;
  mapUserToJson(user: User): any;
  mapPhotoLinksToJson(links: PhotoLinks): any;
}
