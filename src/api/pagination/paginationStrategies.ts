import PaginationOptions from "./paginationOptions";

export enum PaginationStrategy {
  PRODUCTION = "production",
  DEBUG = "debug"
}

export default class PaginationStrategies {
  private items: Map<PaginationStrategy, PaginationOptions>;

  constructor() {
    this.items = new Map();

    this.setItem(PaginationStrategy.PRODUCTION, {
      MAX_PER_PAGE: 30
    });

    this.setItem(PaginationStrategy.DEBUG, {
      MAX_PER_PAGE: 10
    });
  }

  public getItem(strategy: PaginationStrategy): PaginationOptions {
    return this.items[strategy];
  }

  private setItem(strategy: PaginationStrategy, options: PaginationOptions) {
    this.items[strategy] = options;
  }
}
