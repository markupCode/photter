import InvalidArgumentError from "../exceptions/invalidArgumentError";
import Collectable from "./collectable";
import CollectionCache from "./collectionCache";
import Photo from "./photo";

export default class LocalCollection implements Collectable {
  private _path: string;

  get path(): string {
    return this._path;
  }

  set path(path) {
    if (!path) throw new InvalidArgumentError(path);
    this._path = path;
  }

  private _name: string;

  get name(): string {
    return this._name;
  }

  set name(name) {
    if (!name) throw new InvalidArgumentError(name);
    this._name = name;
  }

  private _cache: CollectionCache;

  get cache() {
    return this._cache;
  }

  set cache(cache) {
    if (!cache) throw new InvalidArgumentError(cache);
    this._cache = cache;
  }

  constructor(cache = new CollectionCache(), name?: string, path?: string) {
    this._cache = cache;

    name ? (this.name = name) : (this.name = this.getDefaultName());
    path ? (this.path = path) : (this.path = this.getDefaultPath());
  }

  /**
   * alias to photos from `Cache` **instance**
   */
  get collection(): Map<string, Photo> {
    return this.cache.photos;
  }

  private getDefaultPath(): string {
    return ".";
  }

  private getDefaultName(): string {
    return "default-name";
  }
}
