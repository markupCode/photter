export default class InvalidArgumentError extends Error {
  constructor(arg: any, description?: string) {
    super(getMessage(arg, description));
    this.name = "InvalidArgumentError";
  }
}

function getMessage(arg: any, description: string): string {
  const argType = typeof arg;
  return `arg: ${arg}\ntype: ${argType}\ndescription: ${description}`;
}
