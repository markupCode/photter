export default class CollectionLinks {
  public self: string;
  public html: string;
  public photos: string;
  public related: string;
}
